#include <iostream>
#include<bits/stdc++.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL2_gfx.h>
#include <string>
#include <math.h>
#include <vector>
#include <fstream>
#include <SDL_ttf.h>
using namespace std;
// class definition

SDL_Renderer * m_renderer;
SDL_Texture * background=SDL_CreateTexture(m_renderer,SDL_PIXELFORMAT_RGBA8888,SDL_TEXTUREACCESS_TARGET,1280,800);
const int W = 1280;
const int H = 800;
double scaling_factor= 1;

struct setting
{
    int bg;
    int head;
    int body;
    int sec;
};

class timer
{
private:
    time_t GameStartTime;
    time_t newGameStart;


public:
    timer()
    {
        time(&GameStartTime);
    }

    void setGameStart()
    {
        time(&newGameStart);
    }

    int getLocalElapsedTime ()
    {
        time_t now;
        time(&now);

        return (now-newGameStart);
    }

    int getTotalElapsedTime ()
    {
        time_t now;
        time(&now);

        return (now-GameStartTime);
    }

};

struct motions
{
    int LEFT;
    int RIGHT;
    int JUMP;
    int POW; //special power
    int KICK;
};

class player
{
private:

    const Uint8 *keyboard_state_array = SDL_GetKeyboardState(NULL);
    int r_speed=14; //sprint speed
    int default_jump=53;// default speed
    int j_speed=default_jump; //jump speed
    int gravity=7;

    int frameCount=0;
    int player_num;
    bool jumping =false;
    struct motions controls;

    char specialPower;

    time_t powerUpStartTime;
    time_t localTimeStart;
    time_t stunStartTime;

    void initControls ()
    {
        if (player_num==1)
            controls = {SDL_SCANCODE_A,SDL_SCANCODE_D, SDL_SCANCODE_W,SDL_SCANCODE_LSHIFT,SDL_SCANCODE_S};
        else
            controls= {SDL_SCANCODE_LEFT,SDL_SCANCODE_RIGHT,SDL_SCANCODE_UP,SDL_SCANCODE_RSHIFT,SDL_SCANCODE_DOWN};
    }

    void loadHeadTexture(char skin_code)
    {
        string skn="./Textures/Player/Head/";
        skn.push_back(skin_code);
        skn+=".png";
        skin=IMG_LoadTexture(m_renderer,skn.c_str());
    }

    void loadJerseyTexture(char jersey_code)
    {
        string jrs="./Textures/Player/Body/";
        jrs.push_back(jersey_code);
        jrs+=".png";
        jersey=IMG_LoadTexture(m_renderer,jrs.c_str());

    }

    void powerInit(char c)
    {
        switch (c)
        {
        case 'H':
            specialPower='c'; //clone
            break;

        case 'B':
            specialPower='p'; //punch
            break;

        case 'O':
            specialPower='i'; //invisible ball
            break;

        case 'X':
            specialPower='k'; //kick fireball
            break;

        case 'N':
            specialPower='h'; //head fireball
            break;

        case 'A':
            //random
            string str="cpikh";
            specialPower=str[rand()%(str.length())];
            break;

        }
    }

    void endJump()
    {
        jumping=false;
        j_speed=default_jump;
        ypos=groundheight;
    }

    void clearActivity()
    {
        activity[0]=true;
        for (int i=1; i<6; i++)
            activity[i]=false;
    }


public:

    int head_r=50*scaling_factor; //head radius
    int body_w=56*scaling_factor;
    int body_h=120*scaling_factor;

    //position of sprite's head
    int xpos=120;
    int groundheight=H-body_h-head_r;
    int ypos=groundheight;

    SDL_Texture * skin;
    SDL_Rect skin_rect;
    SDL_Texture * jersey;
    SDL_Rect rect;

    void stun()
    {
        activity[5]=true;
        frameCount=0;

        time(&stunStartTime);
    }

    void resetFrameCount()
    {
        frameCount=0;
    }

    int stunDuration()
    {
        time_t now;
        time(&now);

        SDL_Rect s_rect;
        SDL_Texture* s_texture;

        string address="./Textures/images/";
        address.push_back(frameCount%10+'0');
        address+=".png";

        s_rect.w=176*scaling_factor;
        s_rect.h=69*scaling_factor;
        s_rect.y=H-body_h-head_r-s_rect.h;
        s_rect.x=xpos-s_rect.w/2;

        s_texture=IMG_LoadTexture(m_renderer, address.c_str());
        SDL_RenderCopy(m_renderer,s_texture,NULL,&s_rect);
        frameCount++;

        SDL_DestroyTexture(s_texture);

        return (now-stunStartTime);
    }

    int getPlayerNum ()
    {
        return player_num;
    }

    void setPowerUpStartTime()
    {
        time(&powerUpStartTime);
    }

    int powerUpDuration()
    {
        time_t now;
        time(&now);

        return (now-powerUpStartTime);
    }

    bool activity[6]; ///0: still, 1: run left, 2:run right, 3: kick, 4: powerUp, 5: dazed

    bool inGame=false;

    bool isJumping()
    {
        return jumping;
    }

    void hasJumped()
    {
        jumping=true;
    }

    int max_h()
    {
        return ypos+body_h+head_r;
    }

    int min_h()
    {
        return ypos-head_r;
    }

    int min_w()
    {
        return xpos-body_w/2;
    }

    int max_w()
    {
        return xpos+body_w/2;
    }

    player()
    {
        inGame=false;
    }

    ~player()
    {
        SDL_DestroyTexture(skin);
        SDL_DestroyTexture(jersey);
    }

    player (char skin_code, char jersey_color, int num )
    {
        clearActivity();
        inGame=true;
        player_num=num;
        if (player_num==2)
            xpos=W-xpos;
        loadHeadTexture(skin_code);
        loadJerseyTexture(jersey_color);
        drawPlayer();
        initControls();
        powerInit(skin_code);

        time(&localTimeStart);
    }

    void setLocalTime()
    {
        time(&localTimeStart);
    }

    int localElapsedTime ()
    {
        time_t now;
        time(&now);

        return (now-localTimeStart);
    }

    int getXVel()
    {
        if (activity[0])
            return 0;

        else if (activity[1])
            return (-r_speed);

        else if (activity[2])
            return r_speed;
    }

    int getYVel()
    {
        if (jumping)
            return j_speed;

        return 0;
    }

    char getSpecialPower()
    {
        return specialPower;
    }

    void intersectsWith(player *Opponent)
    {
        //colliding with opponent from the right
        if (this->xpos-(*Opponent).xpos<=body_w && this->xpos-(*Opponent).xpos>0 && (*Opponent).ypos-this->ypos<body_h && (*Opponent).ypos-this->ypos>=0)
        {
            endJump();
            this->xpos=(*Opponent).xpos+body_w+1;
        }

        //colliding with opponent from the left
        else if ((*Opponent).xpos-this->xpos<=body_w && (*Opponent).xpos-this->xpos>0 && (*Opponent).ypos-this->ypos<body_h && (*Opponent).ypos-this->ypos>=0)
        {
            endJump();
            this->xpos=(*Opponent).xpos-body_w-1;
        }
    }

    bool powerBar(int scores[])
    {
        int num_bars;
        int timeFactor=0.2*localElapsedTime();
        Uint32 color;

        if (player_num==1)
        {
            num_bars=min(10,min(4,scores[1]/(scores[0]+1))+int(timeFactor));

            color=(num_bars!=10 || activity[4])? 0xffffffff:(localElapsedTime())%2==0 ? 0xff00ff00:0xffff0000;

            rectangleColor(m_renderer,10,10,310,40,color);
            rectangleColor(m_renderer,15,15,305,35,color);

            for (int i=0; i<num_bars; i++)
                boxColor(m_renderer,15+i*29+1,15,15+(i+1)*29-1,34, color);
        }

        else
        {
            num_bars=min(10,min(4,scores[0]/(scores[1]+1))+int(timeFactor));

            color=num_bars!=10? 0xffffffff:(localElapsedTime())%2==0 ? 0xff00ff00:0xffff0000;

            rectangleColor(m_renderer,W-310,10,W-10,40,color);
            rectangleColor(m_renderer,W-305,15,W-15,35,color);

            for (int i=0; i<num_bars; i++)
                boxColor(m_renderer,W-15-(i+1)*29+1,15,W-15-i*29-1,34, color);
        }

        if (num_bars==10)
            return true;

        return false;

    }

    bool movePlayer(player * Opponent, int scores[])
    {
        if (activity[5])
            return false;

        bool change=false;
        bool canPowerUp=powerBar(scores);

        //kick
        if (keyboard_state_array[this->controls.KICK] && !activity[3])
        {
            activity[3]=true;
            change =true;
        }
        else
            activity[3]=false;

        //go right
        if (keyboard_state_array[this->controls.RIGHT])
        {
            activity[2]=true;
            frameCount++;
            xpos+=r_speed;
            change= true;
        }

        //go left
        else if (keyboard_state_array[this->controls.LEFT])
        {
            activity[1]=true;
            frameCount++;
            xpos-=r_speed;
            change= true;
        }

        //jump
        if (keyboard_state_array[this->controls.JUMP] && !jumping)
        {
            jumping=true;
            change= true;
        }

        //power Up
        if(keyboard_state_array[this->controls.POW] && canPowerUp)
        {
            if (!activity[4])
            {
                setPowerUpStartTime();
                activity[4]=true;
            }

            change =true;
        }

        if (!change)
        {
            if (activity[4])
            {
                clearActivity();
                activity[4]=true;
            }
            else
                clearActivity();

            frameCount=0;
        }

        //check for collisions
        else
        {
            if (!activity[1]&&!activity[2] && !activity[4])
                activity[0]=false;
            intersectsWith(Opponent);
        }

        return change;

    }

    void drawKickRange()
    {
        int num=80;
        int numy=80;

        Sint16 vx[4]= {xpos,xpos+num*scaling_factor,xpos+num*scaling_factor,xpos};
        Sint16 vy[4]= {ypos+numy*scaling_factor,ypos+numy*scaling_factor,max_h()-17*scaling_factor,max_h()-17*scaling_factor};
        filledPolygonColor(m_renderer,vx,vy,4,0x9fffffff);

        Sint16 Vx[4]= {xpos+num*scaling_factor,xpos+2*num*scaling_factor,xpos+2*num*scaling_factor,xpos+num*scaling_factor};
        Sint16 Vy[4]= {max_h()-50*scaling_factor,max_h()-50*scaling_factor,max_h()+20*scaling_factor,max_h()+20*scaling_factor};
        filledPolygonColor(m_renderer,Vx,Vy,4,0x9fffff00);

        SDL_RenderPresent(m_renderer);

        SDL_Event* eve=new SDL_Event();
        while (eve->type!=SDL_KEYDOWN)
            SDL_PollEvent(eve);
    }

    //0 for outside of kick range. 1 for top of the shoe. 2 for middle. 3 for bottom
    int kickRange(int x, int y)
    {
        if (!activity[3])
            return 0;

        int num=100;
        int numy=80;

//        drawKickRange();

        if (player_num==1)
        {
            if (x>=xpos && x<=xpos+num*scaling_factor && y<=max_h()-17*scaling_factor && y>=ypos+numy*scaling_factor)
                return 1;

            else if (x>=xpos+num*scaling_factor && x<=xpos+1.2*num*scaling_factor
                     && y<=max_h()+20*scaling_factor && y>=max_h()-50*scaling_factor)
                return 2;

            else if (x>=xpos && x<=xpos+num*scaling_factor && y<=max_h()+34*scaling_factor && y>=max_h()-17*scaling_factor)
                return 3;

            else
                return 0;
        }
        else
        {
            if (x<=xpos && x>=xpos-num*scaling_factor && y<=max_h()-17*scaling_factor && y>=ypos+numy*scaling_factor)
                return 1;

            else if (x<=xpos-num*scaling_factor && x>=xpos-1.2*num*scaling_factor
                     && y<=max_h()+20*scaling_factor && y>=max_h()-50*scaling_factor)
                return 2;

            else if (x<=xpos && x>=xpos-num*scaling_factor && y<=max_h()+34*scaling_factor && y>=max_h()-17*scaling_factor)
                return 3;

            else
                return 0;
        }


    }


    void drawPlayer()
    {
        skin_rect.x= xpos-head_r;
        skin_rect.y= ypos-head_r;
        skin_rect.w=2*head_r;
        skin_rect.h=2*head_r;
        rect.y= ypos+head_r;
        if (!activity[3] || activity[1] || activity[2])
        {
            rect.x= xpos-body_w/2;
            rect.w=body_w;
        }
        else
        {
            rect.w=body_w+24*scaling_factor;
            rect.x= xpos-rect.w/2;
            if (player_num==1)
                skin_rect.x+=10*scaling_factor;
            else
                skin_rect.x-=10*scaling_factor;
        }
        rect.h=body_h;

        //animate texture
        SDL_Rect srect;
        srect.y=0;
        srect.w=56;
        srect.h=120;

        if (activity[0])
            srect.x=0;

        if (jumping)
        {
            //moving up
            if (j_speed>0)
                srect.x=3*56;
            else
                srect.x=0;

            ypos-=j_speed;
            j_speed-=gravity;

            if (ypos>groundheight)
            {
                activity[0]=true;
                endJump();
            }
        }

        else if( activity[1] || activity[2])
        {
            int realFrame=frameCount/5;
            if (player_num==1)
                skin_rect.x+=10*(realFrame%2);
            else
                skin_rect.x-=10*(realFrame%2);

            skin_rect.y-=5*(realFrame%2);
            rect.y-=5*(realFrame%2);
            srect.x=56*(1+realFrame%2);
        }

        else if (activity[3])
        {
            srect.w+=28;
            srect.x=224;
        }



        if (player_num==1)
        {
            SDL_RenderCopy(m_renderer,jersey,&srect,&rect);
            SDL_RenderCopy(m_renderer,skin,NULL,&skin_rect);
        }
        else
        {
            SDL_RenderCopyEx(m_renderer,jersey,&srect,&rect,0,NULL,SDL_FLIP_HORIZONTAL);
            SDL_RenderCopyEx(m_renderer,skin,NULL,&skin_rect,0,NULL,SDL_FLIP_HORIZONTAL);
        }
    }


//    void clone(player *newPlayer)
//    {
//        newPlayer->skin_rect=this->skin_rect;
//        newPlayer->skin=this->skin;
//        newPlayer->rect=this->rect;
//        newPlayer->jersey=this->jersey;
//        newPlayer->inGame=true;
//        newPlayer->xpos=this->xpos;
//        newPlayer->ypos=this->ypos;
//        for (int i=0;i<5;i++)
//            newPlayer->activity[i]=this->activity[i];
//        newPlayer->player_num=this->player_num;
//    }

};

class image
{
private:
    SDL_Renderer *ren;
    SDL_Rect img_rect;
public:
    string path;
    SDL_Texture *img;
    int img_w, img_h;
    int x;
    int y;
    float sc_x;
    float sc_y;
    void run()
    {
        img = IMG_LoadTexture(ren, path.c_str());
        SDL_QueryTexture(img, NULL, NULL, &img_w, &img_h);
    }

    void load()
    {
        //const char* cpath = path.c_str();


        img_rect.x = x;
        img_rect.y = y;
        img_rect.w = img_w * sc_x;
        img_rect.h = img_h * sc_y;
        SDL_RenderCopy(ren, img, NULL, &img_rect);
        //delete cpath;
    }
    image(string Path, int X, int Y, float SC, SDL_Renderer *Ren)
    {
        x = X;
        y = Y;
        path = Path;
        sc_x = SC;
        sc_y = SC;
        ren = Ren;
        run();
        load();
    }
    void setCenter(int X, int Y)
    {
        x = X - img_w * sc_x / 2;
        y = Y - img_h * sc_y / 2;
        load();
    }
    void render()
    {
        SDL_RenderCopy(ren, img, NULL, &img_rect);
    }
    ~image()
    {
        SDL_DestroyTexture(img);
    }
};
class btn
{
private:
    int x;
    int y;
    int he;
    int wi;
public:
    image *pic;
    btn(int X, int Y, string Path, float SC,SDL_Renderer *Ren)
    {
        pic = new image(Path, X, Y, SC, Ren);
        x = X;
        y = Y;
        he = pic->img_h;
        wi = pic->img_w;
    }
    ~btn()
    {
        delete pic;
    }
    void setXY(int X, int Y)
    {
        x = X;
        y = Y;
        pic->x = X;
        pic->y = Y;
        pic->load();
    }
    void setCenter(int X, int Y)
    {
        x = X - wi * pic->sc_x / 2;
        y = Y - he * pic->sc_y / 2;
        pic->x = x;
        pic->y = y;
        pic->load();
    }
    bool btn_clicked(int mouse_x, int mouse_y)
    {
        if(mouse_x >= x && mouse_x <= x + wi * pic->sc_x
                && mouse_y >= y && mouse_y <= y + he * pic->sc_y)
            return true;
        else
            return false;
    }

};
struct plyr
{
    string name;
    int score;
};
class playerInfo
{
private:
    SDL_Renderer *ren;
    SDL_Texture *mTexture;
    SDL_Rect img_rect;
    //file.open("db.txt", ios::out | ios::in );
    vector<plyr> plist;
    int exists(string name)
    {
        for(int i = 0; i < plist.size(); i++)
        {
            if(plist.at(i).name == name)
                return i;
        }
        return -1;
    }
    string split(int st, int fi, string str)
    {
        string ret = "";
        for(; st <= fi; st++)
            ret += str[st];
        return ret;
    }
    plyr infosplit(string str)
    {
        struct plyr p1;
        int st = 0;
        for(int i = 0; i < str.length(); i++)
        {
            if(str[i] == ',')
            {
                p1.name = split(0, i - 1, str);
                st = i + 1;
            }
        }
        p1.score = stoi(split(st, str.length() - 1, str));
        return p1;
    }
public:
    struct plyr p1;
    struct plyr p2;
    playerInfo()
    {
        ifstream file("db.txt");
        string tmp;
        while(getline(file, tmp))
        {
            if(file.good())
            {
                struct plyr pl = infosplit(tmp);
                plist.push_back(pl);
            }
        }
        file.close();
    }
    void addPlayer1(string name)
    {
        int id = exists(name);
        if(id == -1)
        {
            p1.name = name;
            p1.score = -1;
            plist.push_back(p1);
        }
        else
        {
            p1 = plist[id];
        }
    }

    void addPlayer2(string name)
    {
        int id = exists(name);
        if(id == -1)
        {
            p2.name = name;
            p2.score = -1;
            plist.push_back(p2);
        }
        else
        {
            p2 = plist[id];
        }
    }
    void update(string name, int score)
    {
        int id = exists(name);
        if(id != -1)
        {
            plist[id].score = score;
        }
    }
    void del(string name)
    {
        int id = exists(name);
        if(id != -1)
        {
            plist.erase(plist.begin() + id);
        }
    }

    int leaderboardItem(SDL_Renderer *Ren)
    {
        sortlist();
        ren = Ren;
        mTexture = SDL_CreateTexture(ren, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_TARGET, 490, (plist.size() - 2) * 50);
        SDL_SetRenderTarget(ren, mTexture);
        SDL_SetTextureBlendMode(mTexture, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(ren, 0, 0, 0, 0);
        SDL_RenderClear(ren);
        int x = 20,y = 0, he = 40;

        int j = 0;
        for(int i = plist.size() - 1; i >= 0 ; i--)
        {
            if(plist[i].score != -1)
            {
                textRGBA(ren, x, y, plist[i].name.c_str(), 1, 45, 0, 0, 0, 255);
                textRGBA(ren, x + 400, y, to_string(plist[i].score).c_str(), 1, 45, 0, 0, 0, 255);
                y += he;
            }
        }
        SDL_SetRenderTarget(ren, NULL);
        return y - he * 2;
    }

    void leaderboardItemRender(int X, int Y, int st_y)
    {
        img_rect.w = 490;
        img_rect.h = 220;
        img_rect.x = X;
        img_rect.y = Y;
        SDL_Rect part;
        part.x = 0;
        part.y = st_y;
        part.w = 490;
        part.h = 220;
        SDL_RenderCopy(ren, mTexture, &part, &img_rect);
    }
    void save_list()
    {
        sortlist();
        ofstream file("db.txt", std::ios::out | std::ios::trunc);
        for(int i = 0; i < plist.size(); i++)
            file<<plist[i].name<<","<<plist[i].score<<endl;
    }
    // :( sort based on score
    void sortlist()
    {
        for(int i = 0; i < plist.size(); i++)
        {
            for(int j = i + 1; j < plist.size(); j++)
            {
                if(plist[i].score > plist[j].score)
                    swap(plist[i], plist[j]);
            }
        }
    }
};



class ball
{
private:

    SDL_Texture * skin;
    SDL_Point center;

    //0 for no fireball, 1 for player one, 2 for player two
    int fireball=0;

    int omega=10;

    double angle;

    //position of center
    int startpos=300;

    //velocity
    double xvel=0;
    double yvel=0;

    int defaultKickSpeed=81;
    int kickSpeed=81;
    int default_skin;

    //acceleration
    int gravity=5;
    double friction=2;

    int yground=670;

    void loadSkin(int i)
    {
        string skn="./Textures/Ball/";
        skn.push_back('0'+i);
        skn+=".png";
        skin=IMG_LoadTexture(m_renderer,skn.c_str());
    }

    //distance between ball and a given point
    double dist (int x, int y)
    {
        return sqrt((xpos-x)*(xpos-x)+(ypos-y)*(ypos-y));
    }

public:

    int xpos=W/2;
    int ypos=startpos;
    int radius=40*scaling_factor;

    ball();
    ~ball()
    {
        SDL_DestroyTexture(skin);
    }

    ball(int code,player * p_list)
    {
        loadSkin(code);
        default_skin=code;
        angle=0;
        draw(p_list);
    }

    void setXvel(int v)
    {
        xvel=v;
    }

    void setYvel(int v)
    {
        yvel=v;
    }

    void Rotate()
    {
        if (velocity()>=4)
            angle+=velocity()*omega*0.3;

        if (angle>=360)
            angle-=360;
    }

    void setFireball (int num)
    {
        fireball=num;
    }

    void invisible(bool toggle)
    {
        if (toggle)
        {
            loadSkin(0);
        }
        else
            loadSkin(default_skin);
    }

    void drawGoals()
    {
        SDL_Texture * goalPost;
        goalPost=IMG_LoadTexture(m_renderer,"./Textures/g.png");

        SDL_Rect g_rect;
        g_rect.w=100*scaling_factor;
        g_rect.h=360*scaling_factor;
        g_rect.y=H-360*scaling_factor;

        g_rect.x=0;
        SDL_RenderCopy(m_renderer,goalPost,NULL,&g_rect);

        g_rect.x=W-g_rect.w;
        SDL_RenderCopyEx(m_renderer,goalPost,NULL,&g_rect,0,NULL,SDL_FLIP_HORIZONTAL);

    }

    void draw(player *p_list)
    {
        Rotate();
        for (int i=0; i<6; i++)
        {
            if (p_list[i].inGame)
            {
                kickBall(&p_list[i]);
            }
        }
        changePos();
        for (int i=0; i<6; i++)
        {
            if (p_list[i].inGame)
            {
                reflect(&p_list[i]);
            }
        }
        squeezeOut(&p_list[0],&p_list[1]);


        SDL_Rect skn_rect;
        skn_rect.h=2*radius;
        skn_rect.w=2*radius;
        skn_rect.x=xpos-radius;
        skn_rect.y=ypos-radius;

        center.x=radius;
        center.y=radius;

        SDL_RenderCopyEx(m_renderer,skin,NULL,&skn_rect,angle,&center, SDL_FLIP_NONE);
        drawGoals();
    }

    //returns 0 for no goal,-1 for out, 1 for player 1, 2 for player 2
    int isGoal()
    {
        if (xpos-radius<100*scaling_factor)
        {
            if (ypos+radius<=360*scaling_factor)
                return -1;

            return 2;
        }
        else if (xpos+radius>W-100*scaling_factor)
        {
            if (ypos+radius<=360*scaling_factor)
                return -1;

            return 1;
        }
        else
            return 0;
    }

    double velocity()
    {
        return sqrt(xvel*xvel+yvel*yvel);
    }

    void slowDown()
    {
        if (velocity()!=0)
        {
            xvel-=xvel*friction/velocity();
            yvel-=yvel*friction/velocity();
        }
    }

    //movement
    void changePos()
    {
        xpos+=xvel;
        ypos+=yvel;

        bounce();
        yvel+=gravity;
        slowDown();

    }

    //bounce off of ground
    void bounce ()
    {
        if (ypos+radius>=H && yvel>0)
        {
            //change threshold if necessary
            if (yvel<=5)
                yvel=0;
            else
                yvel=-yvel;

            ypos=H-radius;

            //maybe add particle engine(grass)
        }
    }

    //reflect from player
    void reflect (player * m_player)
    {
        double d=dist(m_player->xpos,m_player->ypos);
        //reflect from head
        if (d<=m_player->head_r+radius)
        {
            int VXrel=xvel-m_player->getXVel();
            int VYrel=yvel-m_player->getYVel();
            int x_rel=this->xpos-m_player->xpos;
            int y_rel=this->ypos-m_player->ypos;

            //add elasticity coefficient if necessary
            xvel=xvel-2*(VXrel*x_rel+VYrel*y_rel)*x_rel/(d*d);
            yvel=yvel-2*(VXrel*x_rel+VYrel*y_rel)*y_rel/(d*d);

            this->xpos=m_player->xpos+x_rel*(radius+m_player->head_r+1)/d;
            this->ypos=m_player->ypos+y_rel*(radius+m_player->head_r+1)/d;
        }

        //reflect from body
        else if (this->ypos <= m_player->ypos+m_player->head_r+m_player->body_h
                 && this->ypos >= m_player->ypos+m_player->head_r)
        {
            int vrel=m_player->getXVel()-xvel;
            if(this->xpos-radius<=m_player->xpos+m_player->body_w/2+vrel && this->xpos-radius>m_player->xpos-m_player->body_w/2 && vrel>=0)
            {
                xvel=-xvel+2*m_player->getXVel();
                this->xpos=m_player->xpos+m_player->body_w/2+radius+1;

                if (fireball==m_player->getPlayerNum())
                {
                    if (vrel>0)
                    {
                        m_player->stun();
                        xvel/=100;
                        m_player->xpos-=200*scaling_factor;
                    }
                }
            }
            else if (this->xpos+radius>=m_player->xpos-m_player->body_w/2+vrel && this->xpos+radius < m_player->xpos+m_player->body_w/2 && vrel<=0)
            {
                xvel=-xvel+2*m_player->getXVel();
                this->xpos=m_player->xpos-m_player->body_w/2-radius-1;

                if (fireball==m_player->getPlayerNum())
                {
                    if (vrel<0)
                    {
                        m_player->stun();
                        xvel/=100;
                        m_player->xpos+=200 * scaling_factor;
                    }
                }
            }
        }

        //reflect from foot while jumping
        else if (m_player->isJumping() && this->xpos <= m_player->xpos+m_player->body_w/2
                 && this->xpos >= m_player->xpos-m_player->body_w/2 && this->ypos-radius <= m_player->ypos + m_player->head_r +m_player->body_h)
        {
            yvel=-yvel+2*m_player->getYVel();
            this->ypos=m_player->ypos+m_player->head_r+m_player->body_h+radius+1;
        }
    }

    int getKickSpeed ()
    {
        return (kickSpeed);
    }

    void kickBall(player * m_player)
    {
        int state=m_player->kickRange(xpos,ypos);

        if (state==0)
            return;

        if (fireball!=m_player->getPlayerNum()&& fireball!=0)
        {
            state=2;
            kickSpeed*=1.5;
        }
        else
            kickSpeed=defaultKickSpeed;

        if (state==1)
        {
            yvel=kickSpeed*sin(50*M_PI/180);
            xvel=kickSpeed*cos(50*M_PI/180);
            if (m_player->xpos > xpos)
                xvel=-xvel;
        }
        else if (state==2)
        {
            yvel=kickSpeed*sin(15*M_PI/180);
            xvel=kickSpeed*cos(15*M_PI/180);
            if (m_player->xpos > xpos)
                xvel=-xvel;
        }
        else
        {
            yvel=-kickSpeed*sin(45*M_PI/180);
            xvel=kickSpeed*cos(45*M_PI/180);
            if (m_player->xpos > xpos)
                xvel=-xvel;
        }
    }

    bool isStuck(player * p_1, player * p_2)
    {
        if (this->ypos >= max(p_1->min_h(),p_2->min_h()) && this->ypos <= min(p_1->max_h(),p_2->max_h())
                && max(p_1->min_w(),p_2->min_w())-min(p_1->max_w(),p_2->max_w())<=2*radius)
        {
            if (this->xpos<=max(p_1->min_w(),p_2->min_w()) && this->xpos>=min(p_1->max_w(),p_2->max_w()))
            {
                return true;
            }
        }

        return false;
    }

    //if ball gets stuck between players
    void squeezeOut (player * p_1, player * p_2)
    {
        if (isStuck(p_1,p_2))
        {
            xvel=0;
            yvel=0;
//            if (p_1->max_w()<p_2->min_w())
//            {
//                int delta=2*radius+2-p_2->min_w()+p_1->max_w();
//                delta/=2;
//                p_2->xpos+=delta;
//                p_1->xpos-=delta;
//            }
//
//            else
//            {
//                int delta=2*radius+2-p_1->min_w()+p_2->max_w();
//                delta/=2;
//                p_1->xpos+=delta;
//                p_2->xpos-=delta;
//            }
////            p_2->xpos-=p_2->getXVel();
////            p_1->xpos-=p_1->getXVel();

            ypos=startpos;

        }
    }
};

// function definition
float ease_bounceOut(float start, float final, int time, int total);
float ease_circ_in(int start_point, int end_point, int current_time, int total_time);
float easeBackOut(float p1, float p2, int time, int totalTime, float overshoot);
float easeBackIn(float p1, float p2, int time, int totalTime, float overshoot);
void init (SDL_Window * m_window);
int splashScreen(playerInfo *PI);
void update();
void punch (player* p_list, int index);
void invisibleBall (ball * B, player * P);
void scoreBoard(int scores[], int e_time);
void loadBG (int i);
void goalScored();
void headFireball (player * p_list, int index, ball * B);
void kickFireball(player * p_list, int index, ball * B);
void powerUp(player * p_list, ball * b);
void BallOut();
void unStun (player *p_list);
void clone(player* p_list, int index);
void resetPlayerList(player*p_list, ball *Ball);
bool newGame(int scores[], timer* time_keeper, int gameDuration, char codes[]);
int startMenu(playerInfo *PI);
void quit(SDL_Window * m_window);
int endMenu(playerInfo *PI, int score1, int score2);
void showText(int x, int y, int width, int height, string text, string fontName, int size, SDL_Color color, int alignVertical, int alignHorizontal, int angle);
setting select(playerInfo *PI);
SDL_Texture leaderboardItem(playerInfo PI);



int main( int argc, char * argv[] )
{
    srand(time(NULL));
    SDL_Window * m_window;
    init(m_window);

    ///game
    // get player information from the file
    int stat = 0;
    playerInfo *PI = new playerInfo();
    int scores[2]= {0,0};
    struct setting pl1;
    struct setting pl2;
    while(stat != -1)
    {
        if(stat == 0)
            stat = splashScreen(PI);
        if(stat == 1)
        {
            scores[0] = 0;
            scores[1] = 0;
            stat = startMenu(PI);
        }
        if(stat == 2)
        {
            pl1 = select(PI);
            pl2 = select(PI);
            int time = (pl1.sec + pl2.sec) / 2;
            if(rand() % 2 == 0)
                loadBG(pl1.bg);
            else
                loadBG(pl2.bg);

            char codes[4]= {pl1.head+'0',pl2.head+'0',pl1.body+'0',pl2.body+'0'};

            if (pl1.head>=10)
                codes[0]='A'+pl1.head-10;

            if (pl2.head>=10)
                codes[1]='A'+pl2.head-10;

            //game clock
            timer timeKeeper;
            scores[0] = 0;
            scores[1] = 0;
            ///game
            while (newGame(scores, &timeKeeper, time, codes))
            {
                timeKeeper.setGameStart();
            }
            stat = 3;
        }
        if(stat == 3)
            stat = endMenu(PI, scores[0], scores[1]);
    }

    quit(m_window);
    return 0;

}
void quit(SDL_Window * m_window)
{
    SDL_DestroyWindow( m_window );
    SDL_DestroyRenderer( m_renderer );
    SDL_DestroyTexture(background);
    IMG_Quit();
    SDL_Quit();
}

void loadBG (int i)
{
    string str="./Textures/Background/";
    str.push_back('0'+i);
    str+=".jpg";
    background=IMG_LoadTexture(m_renderer,str.c_str());

    update();
}

void scoreBoard(int scores[], int e_time)
{
    filledCircleColor(m_renderer,W/2-80,40,20,0x88ffffff);
    string str=to_string(scores[0]);
    textRGBA(m_renderer,W/2-80-7.5*str.length(),40-15,str.c_str(),2,30,0,255,0,255);
    filledCircleColor(m_renderer,W/2+80,40,20,0x88ffffff);
    str=to_string(scores[1]);
    textRGBA(m_renderer,W/2+80-7.5*str.length(),40-15,str.c_str(),2,30,0,255,0,255);

    SDL_Color color;
    color.a=255;
    color.r=255;
    color.g=255;
    color.b=255;

    string time_str=to_string(e_time/60)+":"+to_string(e_time%60);

    showText(W/2,40,0,0,time_str,"Lazyfont.ttf",40,color,0,1,0);
}

void update()
{
    SDL_RenderCopy(m_renderer,background,NULL,NULL);
}

void clone(player* p_list, int index)
{
    p_list[index+2].inGame=true;
    p_list[index+4].inGame=true;
    p_list[index+2].ypos=p_list[index].ypos;
    p_list[index+4].ypos=p_list[index].ypos;
    p_list[index+2].xpos=index%2==0 ? p_list[index].xpos-120 : p_list[index].xpos+120;
    p_list[index+4].xpos=index%2==0 ? p_list[index].xpos-240 : p_list[index].xpos+240;

    //end powerUp
    if (p_list[index].powerUpDuration()>=8)
    {
        p_list[index].activity[4]=false;
        p_list[index].setLocalTime();
        p_list[index+2].inGame=false;
        p_list[index+4].inGame=false;
    }
}

void unStun (player *p_list)
{
    for (int i=0; i<2; i++)
    {
        if (p_list[i].activity[5] && p_list[i].stunDuration()>=3)
        {
            p_list->resetFrameCount();
            p_list[i].activity[5]=false;
        }
    }
}

void headFireball (player * p_list, int index, ball * B)
{
    unStun(p_list);
    B->setFireball(2-index);

    if (B->ypos<=H-p_list[index].body_h)
    {
        if (index==0)
        {
            p_list[index].xpos=B->xpos-B->radius-p_list[index].body_w/2;
            p_list[index].ypos=B->ypos;

            double phi=atan((W-100*scaling_factor-B->xpos)/(H-p_list[index].ypos));
            double theta=atan((W-100*scaling_factor-B->xpos)/(H-p_list[index].ypos-360*scaling_factor));

            int k_speed=B->getKickSpeed();

            B->setXvel(k_speed*cos((phi+theta)/2));
            B->setYvel(k_speed*sin((phi+theta)/2));

        }
        else
        {
            p_list[index].xpos=B->xpos+B->radius+p_list[index].body_w/2;
            p_list[index].ypos=B->ypos;

            double phi=atan((-100*scaling_factor+B->xpos)/(H-p_list[index].ypos));
            double theta=atan((-100*scaling_factor+B->xpos)/(H-p_list[index].ypos-360*scaling_factor));

            int k_speed=B->getKickSpeed();

            B->setXvel(-k_speed*cos((phi+theta)/2));
            B->setYvel(k_speed*sin((phi+theta)/2));
        }

        p_list[index].hasJumped();

        B->setFireball(0);
        p_list[index].activity[4]=false;
        p_list[index].setLocalTime();
    }

    if (p_list[index].powerUpDuration()>=8)
    {
        B->setFireball(0);
        p_list[index].activity[4]=false;
        p_list[index].setLocalTime();
    }
}

void kickFireball(player * p_list, int index, ball * B)
{
    unStun(p_list);
    B->setFireball(2-index);

    if (p_list[index].powerUpDuration()>=8)
    {
        B->setFireball(0);
        p_list[index].activity[4]=false;
        p_list[index].setLocalTime();
    }

}

void punch (player* p_list, int index)
{
    if (!p_list[1-index].activity[5])
        p_list[1-index].stun();

    if (p_list[1-index].stunDuration()>=3)
    {
        p_list[index].activity[4]=false;
        p_list[index].setLocalTime();
        unStun(p_list);

    }
}

void invisibleBall (ball * B, player * P)
{
    B->invisible(true);

    if (P->powerUpDuration()>=3)
    {
        P->activity[4]=false;
        P->setLocalTime();
        B->invisible(false);
    }

}

void powerUp(player * p_list, ball * b)
{

    if (p_list[0].activity[4])
    {
        char sp=p_list[0].getSpecialPower();

        switch (sp)
        {
        case 'c':
            clone(p_list,0);
            break;

        case 'p':
            punch (p_list,0);
            break;

        case 'i':
            invisibleBall(b,&p_list[0]);
            break;

        case 'k':
            kickFireball(p_list,0, b);
            break;

        case 'h':
            headFireball(p_list,0,b);
            break;

        }
    }
    if (p_list[1].activity[4])
    {
        char sp=p_list[1].getSpecialPower();

        switch (sp)
        {
        case 'c':
            clone(p_list,1);
            break;

        case 'p':
            punch (p_list,1);
            break;

        case 'i':
            invisibleBall(b,&p_list[1]);
            break;

        case 'k':
            kickFireball(p_list,1, b);
            break;

        case 'h':
            headFireball(p_list,1,b);
            break;

        }
    }
}

void resetPlayerList(player*p_list, ball *Ball)
{
    for (int i=0; i<6; i++)
        delete (&p_list[i]);

    delete (Ball);
}

void goalScored()
{
    SDL_Texture *g_texture;
    SDL_Rect g_rect;

    g_texture=IMG_LoadTexture(m_renderer,"./Textures/goal.png");

    for (int i=1; i<=10; i++)
    {
        g_rect.w=600*scaling_factor*0.1*i;
        g_rect.h=523*scaling_factor*0.1*i;
        g_rect.x=W/2-g_rect.w/2;
        g_rect.y=H/2-g_rect.h/2;
        SDL_RenderCopy(m_renderer,g_texture,NULL,&g_rect);
        SDL_RenderPresent(m_renderer);
        SDL_Delay(50);
    }

    SDL_DestroyTexture(g_texture);
    SDL_Delay(500);
}

void BallOut()
{
    SDL_Texture *g_texture;
    SDL_Rect g_rect;

    g_texture=IMG_LoadTexture(m_renderer,"./Textures/out.png");


    for (int i=1; i<=10; i++)
    {
        g_rect.w=469*scaling_factor*0.1*i;
        g_rect.h=378*scaling_factor*0.1*i;
        g_rect.x=W/2-g_rect.w/2;
        g_rect.y=H/2-g_rect.h/2;
        SDL_RenderCopy(m_renderer,g_texture,NULL,&g_rect);
        SDL_RenderPresent(m_renderer);
        SDL_Delay(50);
    }
    SDL_DestroyTexture(g_texture);
    SDL_Delay(500);
}

bool newGame(int scores[], timer* time_keeper, int gameDuration, char codes[])
{
    int fps=60;
    player *playerList=new player[6] {{codes[0],codes[2],1},{codes[1],codes[3],2},{codes[0],codes[2],1},{codes[1],codes[3],2},{codes[0],codes[2],1},{codes[1],codes[3],2}};

    for (int i=2; i<6; i++)
        playerList[i].inGame=false;

    ball Ball (1,playerList);
    SDL_RenderPresent(m_renderer);

    SDL_Event *e=new SDL_Event();
    e->type=NULL;
    while(e->key.keysym.sym!=SDLK_ESCAPE)
    {
        e->type=NULL;
        SDL_PollEvent(e);

        update();

        for (int i=0; i<6; i++)
            if (playerList[i].inGame)
                playerList[i].movePlayer(&playerList[1-i%2],scores);
        if (e->key.keysym.sym==SDLK_ESCAPE)
        {
            resetPlayerList(playerList, &Ball);
            return false;
        }

        powerUp(playerList, &Ball);

        for (int i=0; i<6; i++)
        {
            if (playerList[i].inGame)
            {
                playerList[i].drawPlayer();
            }
        }
        Ball.draw(playerList);
        scoreBoard(scores, time_keeper->getTotalElapsedTime());

        if (Ball.isGoal()!=0)
        {
            if (Ball.isGoal()==-1)
            {
                BallOut();
                return true;
            }

            scores[Ball.isGoal()-1]++;

            update();

            for (int i=0; i<6; i++)
            {
                if (playerList[i].inGame)
                {
                    playerList[i].drawPlayer();
                }
            }
            Ball.draw(playerList);
            scoreBoard(scores, time_keeper->getTotalElapsedTime());

            goalScored();

            resetPlayerList(playerList, &Ball);
            return true;
        }

        SDL_RenderPresent(m_renderer);
        SDL_Delay(1000/fps);

        if (time_keeper->getTotalElapsedTime()>=gameDuration)
        {
            resetPlayerList(playerList,&Ball);
            return false;
        }
    }
}


void init (SDL_Window * m_window)
{
    Uint32 SDL_flags = SDL_INIT_VIDEO | SDL_INIT_TIMER ;
    Uint32 WND_flags = SDL_WINDOW_SHOWN;

    SDL_Init( SDL_flags );
    SDL_CreateWindowAndRenderer( 1280, 800, WND_flags, &m_window, &m_renderer );
    //Pass the focus to the drawing window
    SDL_RaiseWindow(m_window);

    // Clear the window with a black background
    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
    SDL_RenderClear( m_renderer );

    SDL_RenderPresent( m_renderer );
}

int splashScreen(playerInfo *PI)
{
    image *bg = new image("pic/inputbg.jpg", 0, 0, 1.0, m_renderer);
    bg->sc_x = 1.5023474178403755868544600938967;
    bg->sc_y = 1.6666666666666666666666666666667;
    bg->load();
    SDL_Event *e = new SDL_Event();
    int fps = 1000 / 15;
    string player1 = "", player2 = "";
    // text input one
    SDL_StartTextInput();
    int time = 1;
    while(true)
    {
        SDL_PollEvent(e);
        if(e->key.keysym.sym == SDLK_RETURN)
            break;
        if(time % 5 == 0)
        {
            time = 1;
            if(player1[player1.length() - 1] == '|')
            {
                player1.pop_back();
            }
            else
            {
                player1 += "|";
            }
        }
        if(e->type == SDL_KEYDOWN && e->key.keysym.sym == SDLK_BACKSPACE && player1.length() > 0)
        {
            if(player1[player1.length() - 1] == '|')
            {
                player1.pop_back();
                player1.pop_back();
                player1 += '|';
            }
            else
            {
                player1.pop_back();
            }
            e->type = NULL;
        }
        if(e->type == SDL_TEXTINPUT)
        {
            if(player1[player1.length() - 1] == '|')
            {
                player1.pop_back();
                player1 += e->text.text;
                player1 += '|';
            }
            else
            {
                player1 += e->text.text;
            }
        }
        if(player1 != "")
            textRGBA(m_renderer, 200, 600, player1.c_str(), 1, 45, 255, 255, 255, 255);
        SDL_RenderPresent( m_renderer );
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear( m_renderer );
        bg->render();
        time++;
    }
    if(player1[player1.length() - 1] == '|')
        player1.pop_back();
    time = 1;
    SDL_StartTextInput();
    e->type = 0;

    while(true)
    {
        SDL_PollEvent(e);
        if(e->type == SDL_KEYDOWN && e->key.keysym.sym == SDLK_RETURN)
            break;
        if(time % 5 == 0)
        {
            time = 1;
            if(player2[player2.length() - 1] == '|')
            {
                player2.pop_back();
            }
            else
            {
                player2 += "|";
            }
        }
        if(e->type == SDL_KEYDOWN && e->key.keysym.sym == SDLK_BACKSPACE && player2.length() > 0)
        {
            if(player2[player2.length() - 1] == '|')
            {
                player2.pop_back();
                player2.pop_back();
                player2 += '|';
            }
            else
            {
                player2.pop_back();
            }
        }
        if(e->type == SDL_TEXTINPUT)
        {
            if(player2[player2.length() - 1] == '|')
            {
                player2.pop_back();
                player2 += e->text.text;
                player2 += '|';
            }
            else
            {
                player2 += e->text.text;
            }
        }
        textRGBA(m_renderer, 200, 600, player1.c_str(), 1, 45, 255, 255, 255, 255);
        if(player2 != "")
            textRGBA(m_renderer, 880, 600, player2.c_str(), 1, 45, 255, 255, 255, 255);
        SDL_RenderPresent( m_renderer );
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear( m_renderer );
        bg->render();
        time++;
    }
    if(player2[player2.length() - 1] == '|')
        player2.pop_back();
    bool clicked = false;
    btn *letgo = new btn(W / 2 - 66, H / 2 - 66 - 100, "pic/letgo.png", 0.5, m_renderer);
    letgo->pic->render();
    textRGBA(m_renderer, 200, 600, player1.c_str(), 1, 45, 255, 255, 255, 255);
    textRGBA(m_renderer, 880, 600, player2.c_str(), 1, 45, 255, 255, 255, 255);
    SDL_RenderPresent( m_renderer );
    int mx, my;
    float dsc = -0.02, st_sc = 0.5, fi_sc = 1, sc = st_sc;
    bool anime = true;
    time = 0;
    int total_time = fps * 0.5;
    while(!clicked)
    {
        SDL_PollEvent(e);
        mx = e->motion.x;
        my = e->motion.y;
        if(time < total_time && anime)
        {
            sc = ease_bounceOut(st_sc, fi_sc, time, total_time);
            //cout<<sc<<endl;
            time ++;
        }
        if(time >= total_time || !anime)
        {
            anime = false;
            sc += dsc;
            sc = max((double)sc, 0.5);
            time --;
        }
        if(time <= 0)
        {
            time = 0;
            anime = true;
        }
        if(letgo->btn_clicked(mx, my) && e->type == SDL_MOUSEBUTTONDOWN)
            break;
        letgo->pic->sc_x = sc;
        letgo->pic->sc_y = sc;
        letgo->setXY(W / 2 - 135 * sc, H / 2 - 135 * sc - 100);
        textRGBA(m_renderer, 200, 600, player1.c_str(), 1, 45, 255, 255, 255, 255);
        textRGBA(m_renderer, 880, 600, player2.c_str(), 1, 45, 255, 255, 255, 255);
        SDL_RenderPresent( m_renderer );
        SDL_Delay(fps + 10);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear( m_renderer );
        bg->render();
    }
    SDL_StopTextInput();
    PI->addPlayer1(player1);
    PI->addPlayer2(player2);
    delete bg;
    delete e;
    delete letgo;
    delete e;
    return 1;
}

void aboutUs()
{

    image *bg = new image("pic/inputbg.jpg", 0, 0, 1.0, m_renderer);
    image *popup = new image("pic/popup.png", 100, 100, 0,m_renderer);
    popup->setCenter(W / 2, H / 2);
    bg->sc_x = 1.5023474178403755868544600938967;
    bg->sc_y = 1.6666666666666666666666666666667;
    bg->load();
    SDL_Event *e = new SDL_Event();
    int fps = 1000 / 20;
    //anime vars
    int time = 0, total = fps / 4;
    float sc = 0;
    SDL_RenderPresent(m_renderer);
    // pop up opening anime
    while(time <= total)
    {
        sc = ease_bounceOut(0, 1.0, time, total);
        popup->sc_x = sc;
        popup->sc_y = sc;
        popup->setCenter(W / 2, H / 2);
        SDL_RenderPresent(m_renderer);
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear(m_renderer);
        bg->render();
        time++;
    }
    time = 0;
    total = fps / 3;
    int cl_x = W - 206, cl_y = 110, cl_st_x = W + 180, cl_fi_x = cl_x;
    int ni_x = 170, ni_y = 220, ni_st_x = -200, ni_fi_x = ni_x;
    int ho_x = 170, ho_y = 440,ho_st_x = -200, ho_fi_x = ho_x;
    int hos_x = 390, hos_y = 530, hos_st_y = H + 100, hos_fi_y = hos_y;
    int nik_x = 390, nik_y = 310, nik_st_y = -100, nik_fi_y = nik_y;
    btn *close = new btn(cl_st_x, cl_y, "pic/close.png", 0.8, m_renderer);
    btn *nika = new btn(ni_fi_x, ni_y, "pic/nika.png", 1.0, m_renderer);
    btn *hosein = new btn(ho_fi_x, ho_y, "pic/hosein.png", 1.0, m_renderer);
    string niname = "Nika Zahedi", honame = "Hosein Anjidani", devtitle = "Developed By: ";
    int ttemp;

    while(time <= total)
    {
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear(m_renderer);
        bg->render();
        popup->render();
        ttemp = easeBackOut(-100, 125, time, total, 1.5);
        textRGBA(m_renderer, 180, ttemp, devtitle.c_str(), 1, 60, 0, 0, 0, 255);
        close->setXY(easeBackOut(cl_st_x, cl_fi_x, time, total, 2.5), cl_y);
        nika->setXY(easeBackOut(ni_st_x, ni_fi_x, time, total, 1.5), ni_y);
        hosein->setXY(easeBackOut(ho_st_x, ho_fi_x, time, total, 1.5), ho_y);
        textRGBA(m_renderer, nik_x, easeBackOut(nik_st_y, nik_fi_y, time, total, 1.5), niname.c_str(), 1, 45, 0, 0, 0, 255);
        textRGBA(m_renderer, hos_x, easeBackOut(hos_st_y, hos_fi_y, time, total, 1.5), honame.c_str(), 1, 45, 0, 0, 0, 255);
        showText(930, ttemp, 0, 0, "Special Thanks To : ", "fonts/BM.ttf", 40, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        ttemp = easeBackOut(W + 200, 930, time, total, 1.5);
        showText(ttemp, 185, 0, 0, "Dr. Arasteh", "fonts/BM.ttf", 45, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 255, 0, 0, "Mohammad Ali Sharifimehr", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 295, 0, 0, "Amir Reza Imani", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 335, 0, 0, "Mohammad Amin Eghlimi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 375, 0, 0, "Sina Safizadeh", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 415, 0, 0, "Taher Abadi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 455, 0, 0, "Moien Makkiyan", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 495, 0, 0, "Zeinab Ghazizade", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 535, 0, 0, "Mohammad Amin Eghlimi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 575, 0, 0, "Ata Akbari", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 615, 0, 0, "Melika Rajabi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        SDL_RenderPresent(m_renderer);
        SDL_Delay(fps);
        time++;
    }
    while(true)
    {
        SDL_PollEvent(e);
        if(e->type == SDL_MOUSEBUTTONDOWN)
        {
            if(close->btn_clicked(e->motion.x, e->motion.y))
                break;
        }
        SDL_Delay(fps);
        e->type = 0;
    }
    time = 0;
    while(time <= total)
    {
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear(m_renderer);
        bg->render();
        popup->render();
        ttemp = easeBackIn(125, -100, time, total, 1.5);
        textRGBA(m_renderer, 180, ttemp, devtitle.c_str(), 1, 60, 0, 0, 0, 255);
        close->setXY(easeBackIn(cl_fi_x, cl_st_x, time, total, 2.5), cl_y);
        nika->setXY(easeBackIn(ni_fi_x, ni_st_x, time, total, 1.5), ni_y);
        hosein->setXY(easeBackIn(ho_fi_x, ho_st_x, time, total, 1.5), ho_y);
        textRGBA(m_renderer, nik_x, easeBackIn(nik_fi_y, nik_st_y, time, total, 1.5), niname.c_str(), 1, 45, 0, 0, 0, 255);
        textRGBA(m_renderer, hos_x, easeBackIn(hos_fi_y, hos_st_y, time, total, 1.5), honame.c_str(), 1, 45, 0, 0, 0, 255);
        showText(930, ttemp, 0, 0, "Special Thanks To : ", "fonts/BM.ttf", 40, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        ttemp = easeBackIn(930, W + 200, time, total, 1.5);
        showText(ttemp, 185, 0, 0, "Dr. Arasteh", "fonts/BM.ttf", 45, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 255, 0, 0, "Mohammad Ali Sharifimehr", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 295, 0, 0, "Amir Reza Imani", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 335, 0, 0, "Mohammad Amin Eghlimi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 375, 0, 0, "Sina Safizadeh", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 415, 0, 0, "Taher Abadi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 455, 0, 0, "Moien Makkiyan", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 495, 0, 0, "Zeinab Ghazizade", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 535, 0, 0, "Mohammad Amin Eghlimi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 575, 0, 0, "Ata Akbari", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        showText(ttemp, 615, 0, 0, "Melika Rajabi", "fonts/BM.ttf", 30, SDL_Color{0, 0, 0, 255}, -1, 1, 0);
        SDL_RenderPresent(m_renderer);
        SDL_Delay(fps);
        time++;
    }
    total = fps / 4;
    time = 0;
    while(time <= total)
    {
        sc = easeBackIn(1.0, 0, time, total, 2.0);
        popup->sc_x = sc;
        popup->sc_y = sc;
        popup->setCenter(W / 2, H / 2);
        SDL_RenderPresent(m_renderer);
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear(m_renderer);
        bg->render();
        time++;
    }
    time = 0;
    delete e;
    delete close;
    delete nika;
    delete hosein;
}

int startMenu(playerInfo *PI)
{
    int ret = -1;
    // init start
    int fps = 1000 / 40;
    //background
    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
    SDL_RenderClear( m_renderer );
    string tutPath [] = {"pic/tut/1.png", "pic/tut/2.png","pic/tut/3.png"};
    int num = 3;
    num--;
    int index = 0;
    image *bg = new image("pic/inputbg.jpg", 0, 0, 1.0, m_renderer);
    bg->sc_x = 1.5023474178403755868544600938967;
    bg->sc_y = 1.6666666666666666666666666666667;
    bg->load();
    SDL_RenderPresent( m_renderer );
    int ic_x=920,ic_y=150,stbtn_x=900
                                  ,stbtn_y=320,rarrow_x=470,rarrow_y=280
                                  ,lerrow_x=70,lerrow_y=280,mouse_x,mouse_y;
    int tut_x = 20, tut_y = 20;
    int rank_x = 70, rank_y = 450;
    bool changeMade = true;
    //anime variables
    int st_tut_x = -725, fi_tut_x = tut_x;
    int st_rank_y = H + 400, fi_rank_y = rank_y;
    int st_ic_x = W + 150, fi_ic_x = ic_x;
    int st_st_x = W + 200, fi_st_x = stbtn_x;
    int st_leftarr_y = H + 150, fi_leftarr_y = lerrow_y; // left row
    int st_rightarr_y = H + 150, fi_rightarr_y = rarrow_y; // right row
    int time = 0, total = fps / 2;
    //end anime var
    SDL_Event *e = new SDL_Event();
    btn *stbtn = new btn(stbtn_x, stbtn_y, "pic/startbtn.png", 200.0 / 512, m_renderer);
    btn *ic = new btn(ic_x, ic_y, "pic/icon.jpg", 0.5, m_renderer);
    btn *leftarr = new btn(lerrow_x,lerrow_y, "pic/leftarrow.png", 225.0 / 512, m_renderer);
    btn *rightarr = new btn(rarrow_x, rarrow_y,"pic/rightarrow.png", 225.0 / 512, m_renderer);
    image *tut[3] {new image(tutPath[0], st_tut_x, tut_y, 1.0, m_renderer),
              new image(tutPath[1], st_tut_x, tut_y, 1.0, m_renderer),
              new image(tutPath[2], st_tut_x, tut_y, 1.0, m_renderer)
    };
    btn *rank = new btn(rank_x, rank_y, "pic/scoreboard.png", 0.66, m_renderer);
    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
    SDL_RenderClear( m_renderer );
    bg->render();
    int tsize = PI->leaderboardItem(m_renderer);
    //init end
    while(time <= total)
    {
        stbtn->setXY(easeBackOut(st_st_x, fi_st_x, time, total, 1.5), stbtn_y);
        ic->setXY(easeBackOut(st_ic_x, fi_ic_x, time, total, 1.5), ic_y);
        leftarr->setXY(lerrow_x, easeBackOut(st_leftarr_y, fi_leftarr_y, time, total, 1.5));
        rightarr->setXY(rarrow_x, easeBackOut(st_rightarr_y, fi_rightarr_y, time, total, 1.5));
        tut[0]->x = easeBackOut(st_tut_x, fi_tut_x, time, total, 1.5);
        tut[1]->x = easeBackOut(st_tut_x, fi_tut_x, time, total, 1.5);
        tut[2]->x = easeBackOut(st_tut_x, fi_tut_x, time, total, 1.5);
        rank->setXY(rank_x, easeBackOut(st_rank_y, fi_rank_y, time, total, 1.5));
        tut[0]->load();
        tut[1]->load();
        tut[2]->load();
        SDL_Event *e = new SDL_Event();
        SDL_RenderPresent( m_renderer );
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear( m_renderer );
        bg->render();
        time++;
    }
    PI->leaderboardItem(m_renderer);
    //PI->leaderboardItemRender(200, 470);
    // craete the texture here for rank item
    // events handling
    bool ren = true, stren = false, icoren = false, lren = false, rren = false, in = false;
    int mx, my;
    int yst = 0;
    while(true)
    {
        SDL_PollEvent(e);
        SDL_PumpEvents();
        SDL_GetMouseState(&mx, &my);
        // hover
        if(stbtn->btn_clicked(mx, my))
        {
            stbtn->pic->sc_x = 220.0 / 512;
            stbtn->pic->sc_y = 220.0 / 512;
            ren = true;
            stren = true;
        }
        else if(stren)
        {
            stbtn->pic->sc_x = 200.0 / 512;
            stbtn->pic->sc_y = 200.0 / 512;
            ren = true;
            stren = false;
        }
        if(leftarr->btn_clicked(mx, my))
        {
            leftarr->pic->sc_x = 250.0 / 512;
            leftarr->pic->sc_y = 250.0 / 512;
            leftarr->pic->load();
            ren = true;
            lren = true;
        }
        else if(lren)
        {
            leftarr->pic->sc_x = 225.0 / 512;
            leftarr->pic->sc_y = 225.0 / 512;
            leftarr->pic->load();
            ren = true;
            lren = false;
        }
        if(rightarr->btn_clicked(mx, my))
        {
            rightarr->pic->sc_x = 250.0 / 512;
            rightarr->pic->sc_y = 250.0 / 512;
            rightarr->pic->load();
            ren = true;
            rren = true;
        }
        else if(rren)
        {
            rightarr->pic->sc_x = 225.0 / 512;
            rightarr->pic->sc_y = 225.0 / 512;
            rightarr->pic->load();
            ren = true;
            rren = false;
        }
        if(ic->btn_clicked(mx, my))
        {
            ic->pic->sc_x = 0.6;
            ic->pic->sc_y = 0.6;
            ic->pic->load();
            ren = true;
            icoren = true;
        }
        else if(icoren)
        {
            ic->pic->sc_x = 0.5;
            ic->pic->sc_y = 0.5;
            ic->pic->load();
            ren = true;
            icoren = false;
        }
        // click handle
        if(e->type == SDL_MOUSEBUTTONDOWN && e->button.clicks == 1)
        {
            if(stbtn->btn_clicked(mx, my))
            {
                // start the game
                ret = 2;
                break;
            }
            if(leftarr->btn_clicked(mx, my))
            {
                // next slide
                index++;
                index = min(index, num);
                ren = true;
            }
            if(rightarr->btn_clicked(mx, my))
            {
                // pre slide
                index--;
                index = max(0, index);
                ren = true;
            }
            if(ic->btn_clicked(mx, my))
            {
                // on icon click
                aboutUs();
            }
        }
        if(rank->btn_clicked(mx, my) || in)
        {
            in = true;

            SDL_PollEvent(e);
            if(e->type == SDL_MOUSEWHEEL)
            {
                yst = max(0, yst + e->wheel.y);
                yst = min(tsize, yst + e->wheel.y);
                //cout<<tsize<<" "<<yst<<endl;
                ren = true;
            }
        }
        if(!rank->btn_clicked(mx, my))
            in = false;

        if(ren)
        {
            SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
            SDL_RenderClear( m_renderer );
            bg->render();
            rank->pic->load();
            tut[index]->load();
            stbtn->pic->load();
            ic->pic->load();
            leftarr->pic->load();
            rightarr->pic->load();
            PI->leaderboardItemRender(100, 490, yst);
            SDL_Delay(fps);
            SDL_RenderPresent( m_renderer );
            ren = false;
        }
        else
            SDL_Delay(fps);
        e->type = 0;
    }
    delete bg;
    delete e;
    delete stbtn;
    delete ic;
    delete leftarr;
    delete rightarr;
    delete tut;
    delete rank;
    delete e;
    return ret;
}

int endMenu(playerInfo *PI, int score1, int score2)
{
    int ret = -1;
    int fps = 1000 / 40;
    //background
    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
    SDL_RenderClear( m_renderer );
    image *bg = new image("pic/inputbg.jpg", 0, 0, 1.0, m_renderer);
    bg->sc_x = 1.5023474178403755868544600938967;
    bg->sc_y = 1.6666666666666666666666666666667;
    bg->load();
    // vars for anime
    int total = fps / 2, time = 0;
    int ic_x = W / 2 - 75, ic_st_y = -80, ic_fi_y = 50;
    int pname1_x = W / 4, pname1_st_x = -200, pname1_fi_x = pname1_x, pname1_y = 150;
    int score1_x = W / 4, score1_st_x = -200, score1_fi_x = score1_x, score1_y = 220;
    int bscore1_x = W / 4, bscore1_st_x = -200, bscore1_fi_x = bscore1_x, bscore1_y = 290;
    int pname2_x = 3 * W / 4, pname2_st_x = W + 200, pname2_fi_x = pname2_x, pname2_y = 150;
    int score2_x = 3 * W / 4, score2_st_x = W + 200, score2_fi_x = score2_x, score2_y = 220;
    int bscore2_x = 3 * W / 4, bscore2_st_x = W + 200, bscore2_fi_x = bscore2_x, bscore2_y = 290;
    int pl_x = W / 2, pl_y = 320, pl_st_y = H + 50, pl_fi_y = pl_y;
    int ho_x = W / 2, ho_y = 500, ho_st_y = H + 100, ho_fi_y = ho_y;
    int ex_x = W / 2, ex_y = 680, ex_st_y = H + 150, ex_fi_y = ex_y;
    float ic_sc = 0.5;
    image *ic = new image("pic/icon.jpg", ic_x, ic_st_y, ic_sc, m_renderer);
    btn *pl_btn = new btn(pl_x, pl_st_y, "pic/playbtn.png", 0.6, m_renderer);
    pl_btn->setCenter(pl_x, pl_st_y);
    btn *ho_btn = new btn(ho_x, ho_st_y, "pic/homebtn.png", 0.6, m_renderer);
    pl_btn->setCenter(ho_x, ho_st_y);
    btn *ex_btn = new btn(ex_x, ex_st_y, "pic/exitbtn.png", 0.6, m_renderer);
    pl_btn->setCenter(ex_x, ex_st_y);
    SDL_Color cl;
    cl.r = 255;
    cl.g = 255;
    cl.b = 255;
    cl.a = 255;
    SDL_RenderPresent(m_renderer);
    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
    SDL_RenderClear( m_renderer );
    bg->render();
    if(PI->p1.score < score1)
    {
        PI->p1.score = score1;
        PI->update(PI->p1.name, score1);
    }
    if(PI->p2.score < score2)
    {
        PI->p2.score = score2;
        PI->update(PI->p2.name, score2);
    }
    while(time <= total)
    {
        ic->y = ease_bounceOut(ic_st_y, ic_fi_y, time, total);
        ic->load();
        showText(easeBackOut(pname1_st_x, pname1_fi_x, time, total, 2.0), pname1_y, 0, 0, PI->p1.name, "fonts/Lazyfont.ttf", 60, cl, 0, 1, 0);
        showText(easeBackOut(score1_st_x, score1_fi_x, time, total, 2.0), score1_y, 0, 0, to_string(score1), "fonts/Lazyfont.ttf", 45, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
        showText(easeBackOut(bscore1_st_x, bscore1_fi_x, time, total, 2.0), bscore1_y, 0, 0, to_string(PI->p1.score), "fonts/Lazyfont.ttf", 37, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
        showText(easeBackOut(pname2_st_x, pname2_fi_x, time, total, 2.0), pname2_y, 0, 0, PI->p2.name, "fonts/Lazyfont.ttf", 60, cl, 0, 1, 0);
        showText(easeBackOut(score2_st_x, score2_fi_x, time, total, 2.0), score2_y, 0, 0, to_string(score2), "fonts/Lazyfont.ttf", 45, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
        showText(easeBackOut(bscore2_st_x, bscore2_fi_x, time, total, 2.0), bscore2_y, 0, 0, to_string(PI->p2.score), "fonts/Lazyfont.ttf", 37, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
        pl_btn->setCenter(pl_x, easeBackOut(pl_st_y, pl_fi_y, time, total, 2.0));
        ho_btn->setCenter(ho_x, easeBackOut(ho_st_y, ho_fi_y, time, total, 2.0));
        ex_btn->setCenter(ex_x, easeBackOut(ex_st_y, ex_fi_y, time, total, 2.0));
        SDL_RenderPresent( m_renderer );
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear( m_renderer );
        bg->render();
        time++;
    }
    SDL_Event *e = new SDL_Event();
    bool ren = true, plren = false, horen = false, exren = false;
    int mx, my;
    while(true)
    {
        SDL_PollEvent(e);
        mx = e->motion.x;
        my = e->motion.y;
        // bover
        if(pl_btn->btn_clicked(mx, my))
        {
            pl_btn->pic->sc_x = .7;
            pl_btn->pic->sc_y = .7;
            pl_btn->setCenter(pl_x, easeBackOut(pl_st_y, pl_fi_y, time, total, 2.0));
            ren = true;
            plren = true;
        }
        else if(plren)
        {
            pl_btn->pic->sc_x = .6;
            pl_btn->pic->sc_y = .6;
            pl_btn->setCenter(pl_x, easeBackOut(pl_st_y, pl_fi_y, time, total, 2.0));
            plren = false;
            ren = true;
        }
        if(ho_btn->btn_clicked(mx, my))
        {
            ho_btn->pic->sc_x = .7;
            ho_btn->pic->sc_y = .7;
            ho_btn->setCenter(ho_x, easeBackOut(ho_st_y, ho_fi_y, time, total, 2.0));
            ren = true;
            horen = true;
        }
        else if(horen)
        {
            ho_btn->pic->sc_x = .6;
            ho_btn->pic->sc_y = .6;
            ho_btn->setCenter(ho_x, easeBackOut(ho_st_y, ho_fi_y, time, total, 2.0));
            horen = false;
            ren = true;
        }
        if(ex_btn->btn_clicked(mx, my))
        {
            ex_btn->pic->sc_x = .7;
            ex_btn->pic->sc_y = .7;
            ex_btn->setCenter(ex_x, easeBackOut(ex_st_y, ex_fi_y, time, total, 2.0));
            ren = true;
            exren = true;
        }
        else if(exren)
        {
            ex_btn->pic->sc_x = .6;
            ex_btn->pic->sc_y = .6;
            ex_btn->setCenter(ex_x, easeBackOut(ex_st_y, ex_fi_y, time, total, 2.0));
            exren = false;
            ren = true;
        }

        if(e->type == SDL_MOUSEBUTTONDOWN && e->button.clicks == 1)
        {
            if(pl_btn->btn_clicked(mx, my))
            {
                ret = 2;
                break;
            }
            if(ho_btn->btn_clicked(mx, my))
            {
                ret = 1;
                break;
            }
            if(ex_btn->btn_clicked(mx, my))
            {
                ret = -1;
                break;
            }
        }
        if(ren)
        {
            SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
            SDL_RenderClear( m_renderer );
            bg->render();
            ic->render();
            showText(pname1_x, pname1_y, 0, 0, PI->p1.name, "fonts/Lazyfont.ttf", 60, cl, 0, 1, 0);
            showText(pname2_x, pname2_y, 0, 0, PI->p2.name, "fonts/Lazyfont.ttf", 60, cl, 0, 1, 0);
            showText(score1_x, score1_y, 0, 0, to_string(score1), "fonts/Lazyfont.ttf", 45, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
            showText(score2_x, score2_y, 0, 0, to_string(score2), "fonts/Lazyfont.ttf", 45, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
            showText(bscore1_x, bscore1_y, 0, 0, to_string(PI->p1.score), "fonts/Lazyfont.ttf", 37, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
            showText(bscore2_x, bscore2_y, 0, 0, to_string(PI->p2.score), "fonts/Lazyfont.ttf", 37, SDL_Color{0, 10, 255, 255}, 0, 1, 0);
            pl_btn->setCenter(pl_x, pl_y);
            ho_btn->setCenter(ho_x, ho_y);
            ex_btn->setCenter(ex_x, ex_y);
            SDL_RenderPresent(m_renderer);
            ren = false;
        }
        SDL_Delay(fps);
    }
    delete bg;
    delete ic;
    delete pl_btn;
    delete ho_btn;
    delete ex_btn;
    delete e;
    PI->save_list();
    return ret;
}

setting select(playerInfo *PI)
{
    struct setting st;
    int fps = 1000 / 20;
    image *bg = new image("pic/inputbg.jpg", 0, 0, 1.0, m_renderer);
    bg->sc_x = 1.5023474178403755868544600938967;
    bg->sc_y = 1.6666666666666666666666666666667;
    bg->load();
    SDL_RenderClear(m_renderer);
    //anime
    int bg_x = 0, bg_y = 50, bg_st_x = -1280, bg_fi_x = bg_x;
    int he_x = 0, he_y = 270, he_fi_x = he_x;
    int bo_x = 0, bo_y = 430, bo_fi_x = bo_x;
    int time = 0, total = fps / 2;
    //
    btn *bgx[4];
    btn *head[12];
    btn *body[5];
    for(int i = 0; i < 4; i++)
        bgx[i] = new btn(bg_st_x + 136 * (i + 1) + 150 * i, bg_y, "pic/bg/" + to_string(i + 1) + ".png", 1.0, m_renderer);
    for(int i = 0; i < 12; i++)
        head[i] = new btn(bg_st_x + 25 * (i + 1) + 80 * i, he_y, "pic/head/" + to_string(i + 1) + ".png", .8, m_renderer);
    for(int i = 0; i < 5; i++)
        body[i] = new btn(bg_st_x + 170 * (i + 1) + 52 * i, he_y, "pic/body/" + to_string(i + 1) + ".png", 1.0, m_renderer);
    while(time <= total)
    {
        bg_x = easeBackOut(bg_st_x, bg_fi_x, time, total, 1.5);
        for(int i = 0; i < 4; i++)
            bgx[i]->setXY(bg_x + 136 * (i + 1) + 150 * i, bg_y);
        for(int i = 0; i < 12; i++)
            head[i]->setXY(bg_x + 25 * (i + 1) + 80 * i, he_y);
        for(int i = 0; i < 5; i++)
            body[i]->setXY(bg_x + 170 * (i + 1) + 52 * i, bo_y);
        SDL_RenderPresent(m_renderer);
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear(m_renderer);
        bg->render();
        time++;
        //cout<<bg_x<<"\t";
    }
    bool flag = false;
    SDL_Event *e = new SDL_Event();
    int mx, my;

    while(!flag)
    {
        SDL_PollEvent(e);
        mx = e->motion.x;
        my = e->motion.y;
        //hover
        if(e->type == SDL_MOUSEBUTTONDOWN)
        {
            for(int i = 0; i < 4; i++)
            {
                if(bgx[i]->btn_clicked(mx, my))
                {
                    st.bg = i+1;
                    bgx[i]->pic->sc_x = 1.2;
                    bgx[i]->pic->sc_y = 1.2;
                    bgx[i]->pic->load();
                    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
                    SDL_RenderClear( m_renderer );
                    bg->render();
                    for(int i = 0; i < 4; i++)
                        bgx[i]->pic->render();
                    for(int i = 0; i < 12; i++)
                        head[i]->pic->render();
                    for(int i = 0; i < 5; i++)
                        body[i]->pic->render();
                    SDL_RenderPresent(m_renderer);
                    flag = true;
                    break;
                }
            }
            SDL_Delay(fps);
        }
    }
    e->type = 0;
    flag = false;
    while(!flag)
    {
        //cout<<"2"<<endl;
        SDL_PollEvent(e);
        mx = e->motion.x;
        my = e->motion.y;
        //hover
        if(e->type == SDL_MOUSEBUTTONDOWN)
        {
            for(int i = 0; i < 12; i++)
            {
                if(head[i]->btn_clicked(mx, my))
                {
                    st.head = i+1;
                    head[i]->pic->sc_x = 1.1;
                    head[i]->pic->sc_y = 1.1;
                    head[i]->pic->load();
                    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
                    SDL_RenderClear( m_renderer );
                    bg->render();
                    for(int i = 0; i < 4; i++)
                        bgx[i]->pic->render();
                    for(int i = 0; i < 12; i++)
                        head[i]->pic->render();
                    for(int i = 0; i < 5; i++)
                        body[i]->pic->render();
                    SDL_RenderPresent(m_renderer);
                    flag = true;
                    break;
                }
            }
        }
        SDL_Delay(fps);
    }
    flag = false;
    e->type = 0;
    while(!flag)
    {
        SDL_PollEvent(e);
        mx = e->motion.x;
        my = e->motion.y;
        //hover
        if(e->type == SDL_MOUSEBUTTONDOWN)
        {
            for(int i = 0; i < 5; i++)
            {
                if(body[i]->btn_clicked(mx, my))
                {
                    st.body = i+1;
                    body[i]->pic->sc_x = 1.2;
                    body[i]->pic->sc_y = 1.2;
                    body[i]->pic->load();
                    SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
                    SDL_RenderClear( m_renderer );
                    bg->render();
                    for(int i = 0; i < 4; i++)
                        bgx[i]->pic->render();
                    for(int i = 0; i < 12; i++)
                        head[i]->pic->render();
                    for(int i = 0; i < 5; i++)
                        body[i]->pic->render();
                    SDL_RenderPresent(m_renderer);
                    flag = true;
                    break;
                }
            }
        }
        SDL_Delay(fps);
    }
    e->type = NULL;
    string sec = "";
    // text input one
    SDL_StartTextInput();
    time = 1;
    while(true)
    {
        SDL_PollEvent(e);
        if(e->key.keysym.sym == SDLK_RETURN)
            break;
        if(time % 5 == 0)
        {
            time = 1;
            if(sec[sec.length() - 1] == '|')
            {
                sec.pop_back();
            }
            else
            {
                sec += "|";
            }
        }
        if(e->type == SDL_KEYDOWN && e->key.keysym.sym == SDLK_BACKSPACE && sec.length() > 0)
        {
            if(sec[sec.length() - 1] == '|')
            {
                sec.pop_back();
                sec.pop_back();
                sec += '|';
            }
            else
            {
                sec.pop_back();
            }
            e->type = NULL;
        }
        if(e->type == SDL_TEXTINPUT)
        {
            if(sec[sec.length() - 1] == '|')
            {
                sec.pop_back();
                sec += e->text.text;
                sec += '|';
            }
            else
            {
                sec += e->text.text;
            }
        }
        if(sec != "")
            textRGBA(m_renderer, W / 2, 600, sec.c_str(), 1, 45, 255, 255, 255, 255);
        SDL_RenderPresent( m_renderer );
        SDL_Delay(fps);
        SDL_SetRenderDrawColor( m_renderer, 0, 0, 0, 255 );
        SDL_RenderClear( m_renderer );
        bg->render();
        for(int i = 0; i < 4; i++)
            bgx[i]->pic->render();
        for(int i = 0; i < 12; i++)
            head[i]->pic->render();
        for(int i = 0; i < 5; i++)
            body[i]->pic->render();
        SDL_RenderPresent(m_renderer);
        time++;
    }
    if(sec[sec.length() - 1] == '|')
        sec.pop_back();
    st.sec = stoi(sec);
    delete bg;
    for(int i = 0; i < 4; i++)
        delete bgx[i];
    for(int i = 0; i < 12; i++)
        delete head[i];
    for(int i = 0; i < 5; i++)
        delete body[i];
    delete e;
    return st;
}

void showText(int x, int y, int width, int height, string text, string fontName, int size, SDL_Color color, int alignVertical, int alignHorizontal, int angle)
{
    TTF_Init();
    TTF_Font *font = TTF_OpenFont((fontName).c_str(), size);
    int textWidth, textHeight;
    TTF_SizeText(font, text.c_str(), &textWidth, &textHeight);
    switch (alignHorizontal)
    {
    case 0:
        x += width - textWidth;
        break;

    case 1:
        x += (width - textWidth) / 2;
        break;
    }
    switch (alignVertical)
    {
    case 0:
        y += (height - textHeight) / 2;
        break;

    case 1:
        y += (height - textHeight);
        break;
    }

    SDL_Rect rectText{x, y, width, height};
    SDL_Surface *textSur = TTF_RenderText_Solid(font, text.c_str(), color);
    SDL_Texture *textTex = SDL_CreateTextureFromSurface(m_renderer, textSur);
    SDL_FreeSurface(textSur);
    SDL_QueryTexture(textTex, nullptr, nullptr, &rectText.w, &rectText.h);
    SDL_RenderCopyEx(m_renderer, textTex, nullptr, &rectText, angle, NULL, SDL_FLIP_NONE);
    SDL_DestroyTexture(textTex);
    TTF_CloseFont(font);
}

float ease_bounceOut(float start, float final, int time, int total)
{
    float c = final - start; // change
    float t = float(time) / total;
    if(t < (1 / 2.75))
    {
        return c * (7.5625 * t * t) + start;
    }
    else if(t < (2 / 2.75))
    {
        t -= (1.5 / 2.75);
        return c * (7.5625 * t * t + .75) + start;
    }
    else if(t < (2.5 / 2.75))
    {
        t -= (2.25 / 2.75);
        return c * (7.5625 * t * t + .9375) + start;
    }
    else
    {
        t -= (2.625 / 2.75);
        return c * (7.5625 * t * t + .984375) + start;
    }
}

float ease_circ_in(int start_point, int end_point, int current_time, int total_time)
{
    int delta = end_point - start_point;
    double t = (double)current_time / (double)total_time;
    return (-1.0 * delta * (sqrt(1.0 - t * t) - 1.0) + start_point);
}
float easeBackOut(float p1, float p2, int time, int totalTime, float overshoot = 1.0)
{
    //opposite of easeInBack; this function implements movement that starts fast and slows down going past the ending keyframe
    //implements a bounce in the animation
    //max velocity in the start
    //By Borna Khodabandeh
    int d = p2 - p1;
    double s = overshoot;
    double t = time / (double)totalTime - 1;
    return d * (t * t * ((s + 1) * t + s) + 1) + p1;
}


float easeBackIn(float p1, float p2, int time, int totalTime, float overshoot = 1.0)
{
    //a function that implements an animation that goes past initial frame and then slowly accelerates ball as it reaches the end
    //this function implements a bounce in the animation
    //max velocity as it reaches the end
    //By Borna Khodabandeh

    float d = p2 - p1;
    double s = overshoot;
    double t = time / (double)totalTime;
    return d * t * t * ((s + 1) * t - s) + p1;
}


